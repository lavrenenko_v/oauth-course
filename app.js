const express = require('express');
const dotenv = require('dotenv');
const connectDB = require('./config/db');
const mongoose = require('mongoose');
const morgan = require('morgan');
const exphbs = require('express-handlebars');
const methodOverride = require('method-override');
const path = require("path");
const passport = require('passport');
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);

// Load config
dotenv.config({path:'./config/config.env'});
require('./config/passport-setup')(passport);


connectDB();

const app = express();

if(process.env.NODE_ENV === 'development'){
    app.use(morgan('dev'));
}

//Sessions
app.use(session({
    secret:process.env.SESSION_SECRET,
    resave:false,
    saveUninitialized:false,
    cookie: {maxAge:48*60*60*1000},
    store: new MongoStore({mongooseConnection:mongoose.connection}),
}));

//Handlebars helpers
const {formatDate, stripTags, truncate, editIcon, select} = require('./helpers/hbs');


//Handlebars
app.engine('.handlebars', exphbs({helpers:{formatDate, stripTags, truncate, editIcon, select}}))
app.set('view engine', 'handlebars');

//Passport middleware
app.use(passport.initialize());
app.use(passport.session());

//Initialize body-parser
app.use(express.urlencoded({extended:false}));
app.use(express.json());

// Method override
app.use(
    methodOverride(function (req, res) {
        if (req.body && typeof req.body === 'object' && '_method' in req.body) {
            // look in urlencoded POST bodies and delete it
            let method = req.body._method
            delete req.body._method
            return method
        }
    })
)

//Set a global variable
app.use(function(req, res, next){
    res.locals.user = req.user || null;
    next();
})

//Static folder
app.use(express.static(path.join(__dirname, 'public')))

//Routes
app.use('/', require('./routes/index'));
app.use('/auth', require('./routes/auth'));
app.use('/stories', require('./routes/stories'));




const PORT = process.env.PORT || 5000;

app.listen(PORT, ()=>console.log(`Server is running in ${process.env.NODE_ENV} mode on port ${process.env.PORT}`));
