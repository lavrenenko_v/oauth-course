const GoogleStrategy = require('passport-google-oauth20').Strategy;
const mongoose = require('mongoose');
const User = require('../models/User');


module.exports = function(passport){
    passport.use(new GoogleStrategy({
        clientID:process.env.GOOGLE_CLIENT_ID,
        clientSecret:process.env.GOOGLE_CLIENT_SECRET,
        callbackURL:'/auth/google/redirect'
    },async(accessToken, refreshToken, profile, done)=>{
        try{
        const user = await User.findOne({googleId:profile.id});

            if(user){
                console.log(`Current user is: ${user}`);
                done(null, user);
            }
            else{
               const user = await User.create({
                   googleId:profile.id,
                   displayName:profile.displayName,
                   firstName: profile.name.givenName,
                   lastName: profile.name.familyName,
                   image: profile.photos[0].value,
                   createdAt:profile.createdAt
               })
                console.log(`New user is: ${user}`);
                done(null, user);
            }
        }
    catch(err){console.log('Error '+ err);}}))

    passport.serializeUser((user, done) => {
        done(null, user.id);
    })

    passport.deserializeUser((id, done) => {
        User.findById(id,(err, user) => done(err, user));
    })

}
